<!DOCTYPE html>
<html>
<head>
<title>docker rizkinug!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Docker to rizkinug!</h1>
<p>If you see this page, the rizkinug repository image web server is successfully installed and
working.</p> <p><h3>Further configuration is required.</h3></p>

<p>For online documentation and support please refer to
<a href="https://hub.docker.com/u/rizkinug">rizkinug docker hub</a>.<br/>

<p><em>Thank you for using rizkinug Repository!.</em></p>
</body>
</html>
