#FROM debian:bullseye-slim
#FROM php:7.4.33-fpm-bullseye
FROM php:8.1-fpm-bullseye

# Install Utilities
#ENV COMPOSER_ALLOW_SUPERUSER 1
RUN apt-get -y update
RUN apt-get install -y apt-utils build-essential ca-certificates curl nano wget xz-utils zip
RUN apt-get -qq update && apt-get -qq -y install bzip2
RUN apt-get install -y chrpath libssl-dev libxft-dev
RUN apt-get install -y libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev
RUN apt-get install -y libargon2-dev libcurl4-openssl-dev libonig-dev libreadline-dev libsodium-dev libsqlite3-dev libssl-dev libxml2-dev zlib1g-dev 

# Install Ruby
#RUN apt-get -y update && apt-get install -y ruby-full
#RUN ruby -v
#RUN gem -v

#Set Timezone
RUN apt-get update \
    &&  apt-get install -y tzdata \
    &&  ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime \
    &&  dpkg-reconfigure --frontend noninteractive tzdata
CMD ["date"]

# Install ppa:ondrej/php PPA
#RUN apt-get install -y software-properties-common
#RUN add-apt-repository ppa:ondrej/php
#RUN apt-get update

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer
RUN composer self-update

# Install Extentions
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions
#RUN install-php-extensions mysqli intl gd ioncube_loader xdebug pdo_mysql pdo_pgsql pdo_sqlsrv redis sqlsrv timezonedb zip gnupg imap csv calendar bz2 mcrypt memcache memcached xlswriter
RUN install-php-extensions bz2 calendar csv exif gd gettext gnupg imagick imap intl ioncube_loader
RUN install-php-extensions json_post jsonpath ldap mailparse mcrypt memcache memcached mongodb mysqli
RUN install-php-extensions odbc opcache pcntl pdo_dblib pdo_mysql pdo_pgsql pdo_sqlsrv pgsql redis
RUN install-php-extensions snmp sqlsrv timezonedb uploadprogress xdebug xsl yaml zip zstd 

#PHP Config
#Show PHP errors on development server.
RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN sed -i -e 's/^error_reporting\s*=.*/error_reporting = E_ALL/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^display_errors\s*=.*/display_errors = On/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^lib.output_compression\s*=.*/zlib.output_compression = Off/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^max_execution_time\s*=.*/max_execution_time = 180/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^max_input_time\s*=.*/max_input_time = 360/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^max_input_vars\s*=.*/max_input_vars = 5000/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^memory_limit\s*=.*/memory_limit = 256M/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^cgi.fix_pathinfo\s*=.*/cgi.fix_pathinfo = 0/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^file_uploads\s*=.*/file_uploads = On/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^post_max_size\s*=.*/post_max_size = 192M/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^upload_max_filesize\s*=.*/upload_max_filesize = 96M/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/^allow_url_fopen\s*=.*/allow_url_fopen = On/' /usr/local/etc/php/php.ini

#Show PHP Version & Extentions
#RUN php -v
#RUN php -m

# Install NGINX
RUN apt update -y
RUN apt install nginx supervisor -y
RUN mkdir -p /var/log/php-fpm/

EXPOSE 80
#EXPOSE 443

#Apply SSL
#RUN mkdir /etc/nginx/ndr-ssl/
#COPY ssl/ /etc/nginx/ndr-ssl/

#Apply Web SSL
COPY web-ndr-ssl /etc/nginx/sites-enabled/default

#Apply Web Tanpa SSL
#COPY web-ndr /etc/nginx/sites-enabled/default

##Jangan diubah
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

#Memasukan Aplikasi Langsung
#COPY panrts23/ /var/www/html/
COPY index.php /var/www/html/index.php

#Info PHP
COPY inpo.php /var/www/html/inpo.php

#Apabila pakai CI
#RUN chmod -R 0755 /var/www/html/writable
#RUN chown -R www-data:www-data /var/www/html/writable

##Last Jangan diubah
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

CMD [ "/usr/bin/supervisord" ]

